<?php

namespace App\Imports\API\Stock;

use App\InvCategory;
use App\MasterInventory;
use App\Unit;
use Maatwebsite\Excel\Concerns\ToModel;
use JWTAuth;

class MasterInventoryImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MasterInventory([
            'name' => $row[2],
            'code_product' => $row[1],
            'unit_id' => Unit::where('name_unit', $row[3])->first()->id,
            'unit_price' => 0000,
            'category_id' => InvCategory::where('category_name', $row[0])->first()->id,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!',
            'insert_by' => JWTAuth::parseToken()->toUser()->id,
            'update_by' => JWTAuth::parseToken()->toUser()->id
        ]);
    }
}
