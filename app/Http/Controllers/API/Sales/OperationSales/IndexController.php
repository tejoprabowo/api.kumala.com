<?php

namespace App\Http\Controllers\API\Sales\OperationSales;

use App\Http\Controllers\Controller;
use App\OperationHistory;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function insertDataOperational(Request $request)
    {
        $operationalHistories = new OperationHistory();
        $operationalHistories->nama_outlet = $request->namaOutlet;
        $operationalHistories->alamat = $request->alamat;
        $operationalHistories->no_telp = $request->noTelp;
        $operationalHistories->kode_pos = $request->kodePos;
        $operationalHistories->keterangan = $request->keterangan;
        $operationalHistories->save();
    }
}
