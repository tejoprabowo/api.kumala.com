<?php

namespace App\Http\Controllers\API\Stock\MasterInventory;

use App\Http\Controllers\Controller;
use App\Imports\API\Stock\MasterInventoryImport;
use App\InvCategory;
use App\MasterInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use JWTAuth;

class IndexController extends Controller
{
    public function uploadByExcel(Request $request)
    {
        // menangkap file excel
        $file = $request->file('dataExcel');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_master_inv', $nama_file);

        // import data
        Excel::import(new MasterInventoryImport, public_path('/file_master_inv/'.$nama_file));

        return response('Data berhasil di upload!', 200);
    }

    public function getData()
    {
        $data = MasterInventory::leftJoin('units', 'units.id', '=', 'master_inventories.unit_id')
            ->leftJoin('inv_categories', 'inv_categories.id', '=', 'master_inventories.category_id')
            ->leftJoin('stocks', 'stocks.id', '=', 'master_inventories.id')
            ->select('inv_categories.category_name', 'master_inventories.name', 'master_inventories.code_product', 'units.name_unit', 'stocks.stock', 'master_inventories.unit_price', 'master_inventories.id')
            ->get();
        return response($data, 200);
    }

    public function getDataByName($category)
    {
        $categoryData = InvCategory::where('category_name', $category)->first();
        if (!$categoryData) {
            $data = MasterInventory::where('category_id', $category)
                ->select('name')
                ->get();
        } else {
            $data = MasterInventory::where('category_id', $categoryData->id)
                ->select('name')
                ->get();
        }

        return response($data, 200);
    }

    public function filterDataByCategory($category)
    {
        $category = InvCategory::where('category_name', $category)->first();
        $data = MasterInventory::leftJoin('units', 'units.id', '=', 'master_inventories.unit_id')
            ->leftJoin('inv_categories', 'inv_categories.id', '=', 'master_inventories.category_id')
            ->leftJoin('stocks', 'stocks.id', '=', 'master_inventories.id')
            ->where('master_inventories.category_id', $category->id)
            ->select('inv_categories.category_name', 'master_inventories.name', 'master_inventories.code_product', 'units.name_unit', 'stocks.stock', 'master_inventories.id')
            ->get();

        return response($data, 200);
    }

    public function saveNewMasterData(Request $request)
    {
        $data = new MasterInventory();
        $data->name = $request->nameProduct;
        $data->code_product = Str::random(7);
        $data->unit_id = $request->unit;
        $data->unit_price = 0;
        $data->category_id = $request->category;
        $data->insert_by = JWTAuth::parseToken()->toUser()->id;
        $data->update_by = JWTAuth::parseToken()->toUser()->id;
        $data->save();

        return response('Data baru berhasil di input!', 200);
    }
}
