<?php

namespace App\Http\Controllers\API\Stock\Packaging;

use App\Http\Controllers\Controller;
use App\MasterInventory;
use App\ProductOnPackaging;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function saveDataPackaging(Request $request)
    {
        $packaging = new ProductOnPackaging();
        $packaging->product_id = MasterInventory::where('name', $request->product)->first()->id;
        $packaging->packaging_id = MasterInventory::where('name', $request->packagingName)->first()->id;
        $packaging->qty_product = $request->qtyProduct;
        $packaging->multiple = $request->multiple;
        if ($request->frontSticker) {
            $packaging->front_sticker = MasterInventory::where('name', $request->frontSticker)->first()->id;
        }
        if ($request->backSticker) {
            $packaging->back_sticker = MasterInventory::where('name', $request->backSticker)->first()->id;
        }
        $packaging->save();

        return response('Data berhasil disimpan!', 200);
    }

    public function getDataProductOnPackaging()
    {
        $data = ProductOnPackaging::leftJoin('master_inventories as mproduct_name', 'mproduct_name.id', '=', 'product_on_packagings.product_id')
            ->leftJoin('master_inventories as mpackaging_name', 'mpackaging_name.id', '=', 'product_on_packagings.packaging_id')
            ->leftJoin('master_inventories as mfront_sticker', 'mfront_sticker.id', '=', 'product_on_packagings.front_sticker')
            ->leftJoin('master_inventories as mback_sticker', 'mback_sticker.id', '=', 'product_on_packagings.back_sticker')
            ->select('mproduct_name.name as product_name', 'mpackaging_name.name as packaging_name', 'mfront_sticker.name as front_sticker', 'mback_sticker.name as back_sticker', 'product_on_packagings.qty_product', 'product_on_packagings.multiple', 'product_on_packagings.id')
            ->get();

        return response($data, 200);
    }
}
