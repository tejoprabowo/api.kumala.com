<?php

namespace App\Http\Controllers\API\Stock\CategoryStock;

use App\Http\Controllers\Controller;
use App\InvCategory;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getDataCategory(Request $request)
    {
        $invCategory = InvCategory::whereIn('category_name', $request->dataCategory)->get();
        return response($invCategory, 200);
    }
}
