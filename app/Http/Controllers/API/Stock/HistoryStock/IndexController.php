<?php

namespace App\Http\Controllers\API\Stock\HistoryStock;

use App\Composition;
use App\DataRoasting;
use App\HistoryStocks;
use App\Http\Controllers\Controller;
use App\MasterInventory;
use App\Stock;
use Illuminate\Http\Request;
use JWTAuth;

class IndexController extends Controller
{
    public function saveBulkStock(Request $request)
    {
        for ($i = 0, $iMax = count($request->items); $i < $iMax; $i++) {
            $masteInv = MasterInventory::where('name', $request->items[$i]['product'])
                ->first();
            $stock = Stock::find($masteInv->id);
            if ($request->status === 'stockIn') {
                if (!$stock) {
                    $stock = new Stock();
                    $stock->id = $masteInv->id;
                    $stock->stock = $request->items[$i]['qty'];
                    $stock->last_update_by = JWTAuth::parseToken()->toUser()->id;
                    $stock->save();

                    $history = new HistoryStocks();
                    $history->product_id = $masteInv->id;
                    $history->stock_last = 0;
                    $history->stock_in = $request->items[$i]['qty'];
                    $history->stock_out = 0;
                    $history->stock_update = $request->items[$i]['qty'];
                    $history->reseller = $request->reseller;
                    $history->description = $request->description;
                    $history->insert_by = JWTAuth::parseToken()->toUser()->id;
                    $history->date = $request->date.' '.$request->time.':'.'00';
                    $history->save();
                } else {
                    $history = new HistoryStocks();
                    $history->product_id = $masteInv->id;
                    $history->stock_last = $stock->stock;
                    $history->stock_in = $request->items[$i]['qty'];
                    $history->stock_out = 0;
                    $history->stock_update = (int)$request->items[$i]['qty'] + (int)$stock->stock;
                    $history->reseller = $request->reseller;
                    $history->description = $request->description;
                    $history->insert_by = JWTAuth::parseToken()->toUser()->id;
                    $history->date = $request->date.' '.$request->time.':'.'00';
                    $history->save();

                    $stock->stock = (int)$request->items[$i]['qty'] + (int)$stock->stock;
                    $stock->save();
                }
            } elseif ($request->status === 'stockOut') {
                $history = new HistoryStocks();
                $history->product_id = $masteInv->id;
                $history->stock_last = $stock->stock;
                $history->stock_in = 0;
                $history->stock_out = $request->items[$i]['qty'];
                $history->stock_update = (int)$stock->stock - (int)$request->items[$i]['qty'];
                $history->reseller = 'NONE';
                $history->description = $request->description;
                $history->status_process_id = $request->statusProcess;
                $history->insert_by = JWTAuth::parseToken()->toUser()->id;
                $history->date = $request->date.' '.$request->time.':'.'00';
                $history->save();

                $stock->stock = (int)$stock->stock - (int)$request->items[$i]['qty'];
                $stock->save();
            } elseif ($request->status === 'barangSiapJual') {

                $composition = Composition::where('kopi_id', $masteInv->id)->get();

                for ($o = 0, $oMax = count($composition); $o < $oMax; $o++) {
                    $stockBahan = Stock::find($composition[$o]['bahan_id']);
                    $masterInv = MasterInventory::find($composition[$o]['bahan_id']);
                    if (!$stockBahan) {
                        return response([
                            'msg' => 'Data stock '.$masterInv['name'].' masih kosong, gagal membuat data!',
                            'status' => 'gagal'
                        ], 200);
                    } elseif ($stockBahan->stock === 0) {
                        return response([
                            'msg' => 'Data stock '.$masterInv['name'].' masih kosong, gagal membuat data!',
                            'status' => 'gagal'
                        ], 200);
                    } else {
                        $stockOut = ((float)$request->items[$i]['qty'] * (float)$composition[$o]['persentase']) / 100;
                        $grToKg = (float)$stockOut / 1000;

                        $historyStockGayo = new HistoryStocks();
                        $historyStockGayo->product_id = $masterInv->id;
                        $historyStockGayo->stock_last = $stockBahan->stock;
                        $historyStockGayo->stock_in = 0;
                        $historyStockGayo->stock_out = $grToKg;
                        $historyStockGayo->stock_update = (float)$stockBahan->stock - (float)$grToKg;
                        $historyStockGayo->reseller = 'NONE';
                        $historyStockGayo->description = 'Bahan di pakai untuk membuat kopi '.$request->items[$i]['product'];
                        $historyStockGayo->status_process_id = 7;
                        $historyStockGayo->insert_by = JWTAuth::parseToken()->toUser()->id;
                        $historyStockGayo->date = $request->date.' '.$request->time.':'.'00';
                        $historyStockGayo->save();

                        $stockBahan->stock = (float)$stockBahan->stock - (float)$grToKg;
                        $stockBahan->save();
                    }
                }

                if (!$stock) {
                    $stock = new Stock();
                    $stock->id = $masteInv->id;
                    $stock->stock = $request->items[$i]['qty'];
                    $stock->last_update_by = JWTAuth::parseToken()->toUser()->id;
                    $stock->save();

                    $history = new HistoryStocks();
                    $history->product_id = $masteInv->id;
                    $history->stock_last = 0;
                    $history->stock_in = $request->items[$i]['qty'];
                    $history->stock_out = 0;
                    $history->stock_update = $request->items[$i]['qty'];
                    $history->reseller = $request->reseller;
                    $history->description = $request->description;
                    $history->status_process_id = $request->statusProcess;
                    $history->insert_by = JWTAuth::parseToken()->toUser()->id;
                    $history->date = $request->date.' '.$request->time.':'.'00';
                    $history->save();
                } else {
                    $history = new HistoryStocks();
                    $history->product_id = $masteInv->id;
                    $history->stock_last = $stock->stock;
                    $history->stock_in = $request->items[$i]['qty'];
                    $history->stock_out = 0;
                    $history->stock_update = (float)$request->items[$i]['qty'] + (float)$stock->stock;
                    $history->reseller = $request->reseller;
                    $history->description = $request->description;
                    $history->status_process_id = $request->statusProcess;
                    $history->insert_by = JWTAuth::parseToken()->toUser()->id;
                    $history->date = $request->date.' '.$request->time.':'.'00';
                    $history->save();

                    $stock->stock = (float)$request->items[$i]['qty'] + (float)$stock->stock;
                    $stock->save();
                }
            }
        }

        if ($request->status === 'stockIn') {
            return response('Data berhasil disimpan!', 200);
        } elseif ($request->status === 'stockOut') {
            return response('Data bahan baku berhasil di process!', 200);
        } elseif ($request->status === 'barangSiapJual') {
            return response([
                'msg' => 'Barang Siap Jual Telah Di Proses!',
                'status' => 'Sukses'
            ], 200);
        }

    }

    public function getPbb()
    {
        $data = HistoryStocks::leftJoin('master_inventories', 'master_inventories.id', '=', 'history_stocks.product_id')
            ->where('history_stocks.status_process_id', 2)
            ->select('master_inventories.name', 'history_stocks.stock_out', 'history_stocks.id')
            ->get();

        return response($data, 200);
    }

    public function completePbb(Request $request)
    {
        $roasted = ltrim($request->name, 'Green Bean');
        $masterInv = MasterInventory::where('name', 'like', '%'.$roasted.'%')
            ->where('category_id', 2)
            ->first();

        $stock = Stock::find($masterInv->id);
        $lastStock = 0;

        if ($stock) {
            $lastStock =  $stock->stock;
        }

        $historyStock = new HistoryStocks();
        $historyStock->product_id = $masterInv->id;
        $historyStock->stock_last = $lastStock;
        $historyStock->stock_in = $request->qty;
        $historyStock->stock_out = 0;
        $historyStock->stock_update = (int)$lastStock + (int)$request->qty;
        $historyStock->description = 'Hasil Pengolahan Dari Bahan Baku';
        $historyStock->status_process_id = 5;
        $historyStock->insert_by = JWTAuth::parseToken()->toUser()->id;
        $historyStock->date = $request->date.' '.$request->time.':'.'00';
        $historyStock->save();

        if (!$stock) {
            $stockNew = new Stock();
            $stockNew->id = $masterInv->id;
            $stockNew->stock = $historyStock->stock_update;
            $stockNew->last_update_by = JWTAuth::parseToken()->toUser()->id;
            $stockNew->save();
        } else {
            $stock->stock = $historyStock->stock_update;
            $stock->last_update_by = JWTAuth::parseToken()->toUser()->id;
            $stock->save();
        }

        $historyStockUpdate = HistoryStocks::find($request->idHistory);
        $historyStockUpdate->status_process_id = 6;
        $historyStockUpdate->save();

        $dataRoasting = new DataRoasting();
        $dataRoasting->history_stock_id = $request->idHistory;
        $dataRoasting->diff = $request->qty;
        $dataRoasting->save();

        return response('Stock '.$masterInv->name.' telah ditambah', 200);
    }

    public function adjustmentStock(Request $request)
    {
        $stock = Stock::find($request->idData);

        if (!$stock) {
            $newHistoryStock = new HistoryStocks();
            $newHistoryStock->product_id = $request->idData;
            $newHistoryStock->stock_last = 0;
            $newHistoryStock->stock_in = $request->stockUpdate;
            $newHistoryStock->stock_out = 0;
            $newHistoryStock->stock_update = $request->stockUpdate;
            $newHistoryStock->reseller = 'NONE';
            $newHistoryStock->description = 'Penyesuaian stock akhir oleh admin';
            $newHistoryStock->status_process_id = 1;
            $newHistoryStock->insert_by = JWTAuth::parseToken()->toUser()->id;
            $newHistoryStock->date = date("Y-m-d H:i:s");;
            $newHistoryStock->save();

            $stock = new Stock();
            $stock->id = $request->idData;
            $stock->stock = $request->stockUpdate;
            $stock->last_update_by = JWTAuth::parseToken()->toUser()->id;
            $stock->save();
        } else {
            if ((float)$stock->stock === (float)$request->stockUpdate) {
                return response('Stock tidak ada perubahan!', 200);
            }
            $newHistoryStock = new HistoryStocks();
            $newHistoryStock->product_id = $request->idData;
            if ((float)$stock->stock > (float)$request->stockUpdate) {
                $newHistoryStock->stock_last = $stock->stock;
                $newHistoryStock->stock_in = 0;
                $newHistoryStock->stock_out = (float)$stock->stock - (float)$request->stockUpdate;
                $newHistoryStock->stock_update = $request->stockUpdate;
            } else if ((float)$stock->stock < (float)$request->stockUpdate) {
                $newHistoryStock->stock_last = $stock->stock;
                $newHistoryStock->stock_in =  (float)$request->stockUpdate - (float)$stock->stock;
                $newHistoryStock->stock_out = 0;
                $newHistoryStock->stock_update = $request->stockUpdate;
            }
            $newHistoryStock->reseller = 'NONE';
            $newHistoryStock->description = 'Penyesuaian stock akhir oleh admin';
            $newHistoryStock->status_process_id = 1;
            $newHistoryStock->insert_by = JWTAuth::parseToken()->toUser()->id;
            $newHistoryStock->date = date("Y-m-d H:i:s");;
            $newHistoryStock->save();

            $stock->id = $request->idData;
            $stock->stock = $request->stockUpdate;
            $stock->last_update_by = JWTAuth::parseToken()->toUser()->id;
            $stock->save();
        }

        return response('Stock berhasil di sesuaikan!', 200);
    }

    public function getDataByDate($date)
    {
        $data = HistoryStocks::leftJoin('master_inventories', 'master_inventories.id', '=', 'history_stocks.product_id')
            ->where('history_stocks.date', 'like', '%'. $date .'%')
            ->select('master_inventories.name', 'history_stocks.stock_last', 'history_stocks.stock_in', 'history_stocks.stock_out', 'history_stocks.stock_update', 'history_stocks.description')
            ->get();

        return response($data, 200);
    }
}
