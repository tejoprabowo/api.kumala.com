<?php

namespace App\Http\Controllers\API\Stock\Mutasi;

use App\HistoryStocks;
use App\Http\Controllers\Controller;
use App\MasterInventory;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getDataMutasi($idItem)
    {
        $masterInv = MasterInventory::find($idItem);
        $historyStock = HistoryStocks::leftJoin('master_inventories', 'master_inventories.id', '=', 'history_stocks.product_id')
            ->where('history_stocks.product_id', $idItem)
            ->select('master_inventories.name', 'history_stocks.date', 'history_stocks.stock_last', 'history_stocks.stock_in', 'history_stocks.stock_out', 'history_stocks.description')
            ->get();

        $data = [
            'mutasi' => $historyStock,
            'mutasi_title' => $masterInv->name
        ];

        return response($data);
    }
}
