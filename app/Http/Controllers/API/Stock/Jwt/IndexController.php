<?php

namespace App\Http\Controllers\API\Stock\Jwt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;
use Namshi\JOSE\JWT;

class IndexController extends Controller
{
    public function checkToken(Request $request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status' => 'Token is Invalid'], 402);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['status' => 'Token is Expired'], 401);
            }else{
                return response()->json(['status' => 'Authorization Token not found'], 400);
            }
        }
    }

    public function getInfo(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response($user, 200);
    }
}
