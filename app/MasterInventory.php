<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterInventory extends Model
{
    protected $fillable = [
        'name',
        'code_product',
        'unit_id',
        'unit_price',
        'category_id',
        'description',
        'insert_by',
        'update_by'
    ];
}
