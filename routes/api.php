<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('password', function () {
//    return \Illuminate\Support\Facades\Hash::make('relasi2019!@#');
//});

Route::group(['middleware' => 'api'], function () {
    Route::group(['prefix' => 'stock'], function () {

        /*
         * User Controller
         * Fitur:
         * 1. Login Logic
         * */
        Route::post('user/login', 'API\Stock\User\IndexController@login');

        Route::group(['middelware' => 'jwt'], function () {

            Route::post('category/stock/get', 'API\Stock\CategoryStock\IndexController@getDataCategory');

            /*
             * Jwt Controller
             * Fitur:
             * 1. Check Token
             * */
            Route::post('user/checkAuth', 'API\Stock\Jwt\IndexController@checkToken');
            Route::post('user/getInfo', 'API\Stock\Jwt\IndexController@getInfo');

            /*
             * Master Inventory Controller
             * */
            Route::post('master/inventory/excel', 'API\Stock\MasterInventory\IndexController@uploadByExcel');
            Route::get('master/inventory', 'API\Stock\MasterInventory\IndexController@getData');
            Route::get('master/inventory/{category}', 'API\Stock\MasterInventory\IndexController@getDataByName');
            Route::get('master/inventory/filter/{idCategory}', 'API\Stock\MasterInventory\IndexController@filterDataByCategory');
            Route::post('master/inventory', 'API\Stock\MasterInventory\IndexController@saveNewMasterData');

            Route::post('stock/history', 'API\Stock\HistoryStock\IndexController@saveBulkStock');
            Route::get('stock/pbb', 'API\Stock\HistoryStock\IndexController@getPbb');
            Route::post('stock/complete/pbb', 'API\Stock\HistoryStock\IndexController@completePbb');
            Route::post('stock/adjustment/stock', 'API\Stock\HistoryStock\IndexController@adjustmentStock');
            Route::get('stock/by_date/{date}', 'API\Stock\HistoryStock\IndexController@getDataByDate');

            Route::get('mutasi/{idItem}', 'API\Stock\Mutasi\IndexController@getDataMutasi');

            Route::post('packaging', 'API\Stock\Packaging\IndexController@saveDataPackaging');
            Route::get('packaging', 'API\Stock\Packaging\IndexController@getDataProductOnPackaging');
        });
    });

    Route::group(['prefix' => 'sales'], function () {
        Route::post('user/login', 'API\Sales\User\IndexController@login');

        Route::group(['middleware' => 'jwt'], function () {
//            Route::post('operational/history', 'API\');
        });
    });
});
