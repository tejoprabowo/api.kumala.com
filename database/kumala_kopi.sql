CREATE TABLE `compositions` (
  `id` int(10) UNSIGNED NOT NULL,
  `kopi_id` int(10) UNSIGNED NOT NULL,
  `bahan_id` int(10) UNSIGNED NOT NULL,
  `persentase` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `compositions` (`id`, `kopi_id`, `bahan_id`, `persentase`, `created_at`, `updated_at`) VALUES
(1, 20, 12, 90.00, '2019-11-26 17:00:00', '2019-11-26 17:00:00'),
(2, 20, 16, 10.00, NULL, NULL),
(3, 21, 12, 20.00, NULL, NULL),
(4, 21, 14, 50.00, NULL, NULL),
(5, 21, 13, 10.00, NULL, NULL),
(6, 21, 16, 20.00, NULL, NULL),
(7, 22, 12, 20.00, NULL, NULL),
(8, 22, 14, 40.00, NULL, NULL),
(9, 22, 13, 10.00, NULL, NULL),
(10, 22, 19, 30.00, NULL, NULL),
(11, 23, 12, 35.00, NULL, NULL),
(12, 23, 13, 15.00, NULL, NULL),
(13, 23, 19, 50.00, NULL, NULL),
(14, 24, 28, 10.00, NULL, NULL),
(15, 24, 33, 90.00, NULL, NULL),
(16, 25, 12, 20.00, NULL, NULL),
(17, 25, 13, 30.00, NULL, NULL),
(18, 25, 15, 10.00, NULL, NULL),
(19, 25, 19, 20.00, NULL, NULL),
(20, 25, 16, 20.00, NULL, NULL),
(21, 26, 18, 90.00, NULL, NULL),
(22, 26, 12, 10.00, NULL, NULL),
(23, 27, 18, 100.00, NULL, NULL),
(33, 28, 11, 100.00, NULL, NULL),
(34, 29, 12, 100.00, NULL, NULL),
(35, 30, 13, 100.00, NULL, NULL),
(36, 31, 14, 100.00, NULL, NULL),
(37, 32, 15, 100.00, NULL, NULL),
(38, 33, 16, 100.00, NULL, NULL),
(39, 34, 17, 100.00, NULL, NULL),
(40, 35, 18, 100.00, NULL, NULL),
(41, 36, 19, 100.00, NULL, NULL);

CREATE TABLE `history_stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `stock_last` float NOT NULL,
  `stock_in` float NOT NULL,
  `stock_out` float NOT NULL,
  `stock_update` float NOT NULL,
  `reseller` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_process_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `insert_by` int(10) UNSIGNED NOT NULL,
  `date` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

CREATE TABLE `inv_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `inv_categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Bahan Baku', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(2, 'Barang Setengah Jadi', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(3, 'Barang Jadi Siap Jual', '2019-10-21 17:00:00', '2019-10-21 17:00:00');

CREATE TABLE `master_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_product` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `unit_price` bigint(20) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `insert_by` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `status_for_sell` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `master_inventories` (`id`, `name`, `code_product`, `unit_id`, `unit_price`, `category_id`, `description`, `insert_by`, `update_by`, `status_for_sell`, `created_at`, `updated_at`) VALUES
(1, 'Green Bean Arabika Gayo', '100002', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(2, 'Green Bean Arabika Kintamani', '100005', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(3, 'Green Bean Arabika Mandailing', '100003', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(4, 'Green Bean Arabika Toraja Sapan', '100007', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(5, 'Green Bean Robusta Dampit', '100008', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(6, 'Green Bean Robusta Gn. Karang Pandeglang', '100054', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(7, 'Green Bean Robusta Lampung', '100009', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(8, 'Green Bean Robusta Lampung Tanggamus', '100006', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(9, 'Green Bean Robusta Temanggung', '100004', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(10, 'Green Been Arabika Bajawa (Flores)', '100001', 1, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(11, 'Roasted Bean Arabika Bajawa (Flores)', '100037', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(12, 'Roasted Bean Arabika Gayo', '100034', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(13, 'Roasted Bean Arabika Kintamani', '100036', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(14, 'Roasted Bean Arabika Mandailing', '100035', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(15, 'Roasted Bean Arabika Toraja Sapan', '100038', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(16, 'Roasted Bean Robusta Dampit', '100033', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(17, 'Roasted Bean Robusta Gn. Karang Pandeglang', '100039', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(18, 'Roasted Bean Robusta Lampung', '100031', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(19, 'Roasted Bean Robusta Temanggung', '100032', 2, 0, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(20, 'Yudistira', '100046', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(21, 'Bima', '100047', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(22, 'Arjuna', '100048', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(23, 'Nakula', '100049', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(24, 'Sadewa', '100050', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(25, 'Kedai Kopi 88 House Blend', '100053', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(26, 'Rotbak 88 Gayo', '100052', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(27, 'Rotbak 88 Vietman', '100051', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(28, 'Single Origin Arabika Bajawa (Flores)', '100044', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(29, 'Single Origin Arabika Gayo', '100042', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(30, 'Single Origin Arabika Kintamani', '100043', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(31, 'Single Origin Arabika Mandailing', '100026', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(32, 'Single Origin Arabika Toraja Sapan', '100025', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(33, 'Single Origin Robusta Dampit', '100041', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(34, 'Single Origin Robusta Gn. karang Pandeglang', '100045', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(35, 'Single Origin Robusta Lampung', '100040', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(36, 'Single Origin RobustaTemanggumng', '100027', 2, 0, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, cumque dicta eum exercitationem hic modi nulla, numquam quidem quis, similique vel voluptate. Animi eos hic in magni, minima nesciunt officia!', 1, 1, 'Y', '2019-10-21 23:32:01', '2019-10-21 23:32:01'),
(37, 'Test 1', 'GYS7bVh', 1, 0, 1, NULL, 1, 1, 'Y', '2019-11-21 01:31:21', '2019-11-21 01:31:21'),
(38, 'Test 2', 'vtFhtbJ', 1, 0, 1, NULL, 1, 1, 'Y', '2019-11-21 01:33:17', '2019-11-21 01:33:17'),
(39, 'Test 3', 'i5V4n8G', 1, 0, 1, NULL, 1, 1, 'Y', '2019-11-21 01:34:07', '2019-11-21 01:34:07');

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
)

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_10_18_040705_create_role_user_table', 1),
(3, '2019_10_18_042731_create_foreign_key_role_user', 1),
(4, '2019_10_18_075130_create_master_inventory_table', 1),
(5, '2019_10_18_075131_create_stock_table', 1),
(6, '2019_10_18_084153_create_unit_table', 1),
(7, '2019_10_18_092340_create_foreign_key_unit_id', 1),
(8, '2019_10_18_092735_create_foreign_insert_by', 1),
(9, '2019_10_18_093048_create_foreign_update_by', 1),
(10, '2019_10_18_093935_create_foreign_key_id', 1),
(11, '2019_10_21_042631_add_field_email_on_user', 1),
(12, '2019_10_21_082654_add_field_code_product_on_master_inventories', 1),
(13, '2019_10_21_090233_create_inv_categories_table', 1),
(14, '2019_10_21_090443_add_column_inv_category_on_master_invetories', 1),
(15, '2019_10_21_090817_add_foreign_key_category_id', 1),
(16, '2019_10_23_033154_create_history_stock_table', 2),
(17, '2019_10_24_040200_change_column_status_process', 3),
(19, '2019_10_31_091907_add_field_date_on_history_stocks', 4),
(21, '2019_11_25_084030_add_field_status_process', 5),
(22, '2019_11_25_084947_create_status_process_table', 6),
(23, '2019_11_25_085559_add_foreign_key_status_process_id', 7),
(24, '2019_11_27_075422_create_compositions_table', 8);

CREATE TABLE `role_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_role` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `role_users` (`id`, `name_role`, `created_at`, `updated_at`) VALUES
(1, 'Developer', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(2, 'Owner', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(3, 'Admin', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(4, 'Staff', '2019-10-21 17:00:00', '2019-10-21 17:00:00');

CREATE TABLE `status_processes` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `status_processes` (`id`, `status_name`, `created_at`, `updated_at`) VALUES
(1, 'Tidak Ada', NULL, NULL),
(2, 'Bahan Baku Ke Bahan Setengah Jadi', NULL, NULL),
(3, 'Bahan Setengah Jadi Ke Bahan Siap Jual', NULL, NULL),
(4, 'Barang Keluar Di Jual', NULL, NULL),
(5, 'Hasil Pengolahan Dari Bahan Baku', NULL, NULL),
(6, 'Process Pengolahan Bahan Baku Selesai', NULL, NULL),
(7, 'Process Pengolahan Barang Siap Jual', NULL, NULL);

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `stock` float NOT NULL,
  `last_update_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `units` (`id`, `name_unit`, `created_at`, `updated_at`) VALUES
(1, 'kg', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(2, 'gr', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(3, 'pcs', '2019-10-21 17:00:00', '2019-10-21 17:00:00');

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_employee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
)

INSERT INTO `users` (`id`, `name`, `id_employee`, `email`, `role_id`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Andre Kurnia toro', 'SG-001', 'andrekurniawantoro@gmail.com', 4, '$2y$10$SYhVIdcihXk13sa39qZckekOEamCoEaKonHct1KTmuGfZfi5dc1m.', '2019-10-21 17:00:00', '2019-10-21 17:00:00');

ALTER TABLE `compositions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kopi_id` (`kopi_id`),
  ADD KEY `bahan_id` (`bahan_id`);

ALTER TABLE `history_stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_stocks_status_process_id_foreign` (`status_process_id`);

ALTER TABLE `inv_categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `master_inventories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `master_inventories_name_unique` (`name`),
  ADD KEY `master_inventories_unit_id_foreign` (`unit_id`),
  ADD KEY `master_inventories_insert_by_foreign` (`insert_by`),
  ADD KEY `master_inventories_update_by_foreign` (`update_by`),
  ADD KEY `master_inventories_category_id_foreign` (`category_id`);

ALTER TABLE `migrations`INSERT INTO `units` (`id`, `name_unit`, `created_at`, `updated_at`) VALUES
(1, 'kg', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(2, 'gr', '2019-10-21 17:00:00', '2019-10-21 17:00:00'),
(3, 'pcs', '2019-10-21 17:00:00', '2019-10-21 17:00:00');
  ADD PRIMARY KEY (`id`);

ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `status_processes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `stocks`
  ADD KEY `stocks_id_foreign` (`id`);

ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_id_employee_unique` (`id_employee`),
  ADD KEY `users_role_id_foreign` (`role_id`);

ALTER TABLE `compositions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

ALTER TABLE `history_stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `inv_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `master_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

ALTER TABLE `role_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `status_processes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `compositions`
  ADD CONSTRAINT `compositions_ibfk_1` FOREIGN KEY (`kopi_id`) REFERENCES `master_inventories` (`id`),
  ADD CONSTRAINT `compositions_ibfk_2` FOREIGN KEY (`bahan_id`) REFERENCES `master_inventories` (`id`);

ALTER TABLE `history_stocks`
  ADD CONSTRAINT `history_stocks_status_process_id_foreign` FOREIGN KEY (`status_process_id`) REFERENCES `status_processes` (`id`);

ALTER TABLE `master_inventories`
  ADD CONSTRAINT `master_inventories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `inv_categories` (`id`),
  ADD CONSTRAINT `master_inventories_insert_by_foreign` FOREIGN KEY (`insert_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `master_inventories_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `master_inventories_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `users` (`id`);

ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_id_foreign` FOREIGN KEY (`id`) REFERENCES `master_inventories` (`id`);

ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role_users` (`id`);
