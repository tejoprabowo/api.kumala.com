<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->integer('stock_last');
            $table->integer('stock_in');
            $table->integer('stock_out');
            $table->integer('stock_update');
            $table->string('reseller', 100)->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('status_process_id')->default(1);
            $table->unsignedInteger('insert_by');
            $table->string('date', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_stocks');
    }
}
