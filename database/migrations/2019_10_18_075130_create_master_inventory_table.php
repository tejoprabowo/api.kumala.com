<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->unique();
            $table->string('code_product', 10)->unique();
            $table->unsignedInteger('unit_id');
            $table->float('unit_price');
            $table->unsignedInteger('category_id');
            $table->text('description')->nullable();
            $table->unsignedInteger('insert_by');
            $table->unsignedInteger('update_by');
            $table->string('status_for_sell', 3)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_inventories');
    }
}
